---
layout: default
title:  "Mayonesa"
date:   2020-02-17 12:10:00 -0400
categories: jekyll update
image: /images/mayo-zanahoria-1.jpg
---
## Mayonesa de zanahoria

~40min | 2 personas **by CyC**

_Receta Vegana_

1. **zanahoria** _3 medianas_
2. *aceite** _1/3 del volumen de las zanahorias _
3. **sal** _1 cuacharadita_
4. **limón** _1 cucharada grande (exprimido)_
5. **Ajo** _1 diente mediano_
6. **minipimer** _1 (JAJAJ)_
7. **vaso para procesar**  _ 1 largo_

:)

![imagen]({{ url.site }}/images/mayo-zanahoria-1.jpg)

* Cortar en trozos y hervir las zanahorias hasta que estén tiernas.(~20 minutos)

* Esperar a que estén frías y procesar en un vaso largo junto con: el aceite, la sal el ajo, y el limón.

* acompañar con milanesas de zucchini o talitas/galletitas..

![imagen]({{ url.site }}/images/mayo-zanahoria-2.jpg)

**disfrutar :3**
