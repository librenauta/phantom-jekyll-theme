---
layout: default
title:  "Panqueques"
date:   2020-04-05 12:48:00 -0400
categories: jekyll update
image: /images/panq0.jpg
---
## Panqueques de legumbres sin TACC <3

**tiempo ~ 40min** | 2 personas **by cyc**

_Receta Vegana_

1. **Arroz Yamaní**  _1/2 taza_
2. **Lentejas** _1/2 taza_
3. **Aceite**

## Accesorios
1. licuadora o minipimer
2. Panquequera
3. Espátula
4.
![pancito]({{ url.site }}/images/panq0.jpg "Panqueques")
![pancito]({{ url.site }}/images/panq1.jpg "Panqueques")

* Dejar hidratando en la heladera 24hs el yamaní y las lentejas con agua (taparlas por completo más 1 dedo por encima) en un recipiente.

* Luego del reposo, colar el agua y licuar/ minipimer las legumbres hidratadas con 2 tazas de agua.

* Volcar en un recipiente y dejar a temperatura ambiente (entre 20° y 25°) por 24 hs. (puede ser cerca de la Cocina/Horno) (si, lleva 2 días de pre-preparación, :P )

* Con un cucharon probar la consistencia, tiene que estar un poco viscosa y volcar una porción de cucharon en la sartén caliente con una fina capa de aceite.

* En promedio tardan más en hacerse que los panqueques tradicionales, el punto justo es no moverlos hasta que se vean secos (básicamente en la sartén se evapora la humedad para lograr el panqueque)

* Esperamos hasta que los bordes estén un poco doraritos y comenzamos con la espatula a probar despegarlos, girarlo 5 minutos y sacarlos.

**~Miam, Disfrutar con un salteado de verduras y con hummus \<3**
