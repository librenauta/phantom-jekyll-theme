---
layout: default
title:  "Milanesas Asadas"
date:   2020-09-10 21:27:00 -0400
categories: jekyll update
image: /images/mila-b7.jpg
---
## Milanesas de berenjena Asadas (hornalla)

**tiempo ~ 20min ** | 2 personas **by cyc**

_receta Vegana_ <3

1. **berenjenas** _2_
2. **pan rallado** _250gr_
3. **oregano** _pizca_
4. **aceite** _un chorrito_

## :O

![mila]({{ url.site }}/images/mila-b1.jpg "milanesas")

## bueno esto es delicioso.

* Primero ponemos una berenjena mediana sobre una hornalla y la vamos rotando durante unos 5/8minutos hasta que este bien quemadita.

* Luego retiramos la cáscara quemada y la partimos a la mitad (es bastante simple hacerlo con las manos) y pisamos para aplastar un poco con un pisa papas (dandole forma de milanesa, un espesor de 5/8 mm masomenos).

* En un plato o cubeta tiramos pan rallado, para tapar vuelta y vuelta las berenjenas asadas

* dorar lado y lado en una sartén anti-adherente con un poco de aceite.

![mila]({{ url.site }}/images/mila-b2.jpg "milanesas")

![mila]({{ url.site }}/images/mila-b3.jpg "milanesas")

![mila]({{ url.site }}/images/mila-b4.jpg "milanesas")

![mila]({{ url.site }}/images/mila-b5.jpg "milanesas")

![mila]({{ url.site }}/images/mila-b6.jpg "milanesas")

![mila]({{ url.site }}/images/mila-b7.jpg "milanesas")

![mila]({{ url.site }}/images/mila-b8.jpg "milanesas")
