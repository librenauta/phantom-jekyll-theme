---
layout: default
title:  "Pulled Mushroom"
date:   2020-07-19 10:50:00 -0400
categories: jekyll update
image: /images/pulled-1.png
---

## Pulled mushroom

~ 75 minutos | 2 personas | **by Flores de fuego**

_tal vez menos tiempo si no son tan torpes y metódicos como yo_

_receta *vegana*_

![pulled]({{ url.site }}/images/pulled-1.png "pulled")

### Para la salsa BBQ casera
~ 40 minutos

1. **Tomates enteros** _ 8 unidades_
2. **Ajo** _3 dientes pequeños_
3. **Agua** _1/2 taza_
4. **Salsa de soya** _2 cucharadas, si encuentran baja en sodio, mejor_
5. **Vinagre de manzana** _2 cucharadas_
6. **Vinagre blanco** _2 cucharadas_
7. **Pimienta recién molida** _al gusto_
8. **Cebolla en polvo** _1 cucharadita, si quieres, puedes sustituirla por cebolla entera_
9. **Azúcar** _1/3 taza_
10. **Aceite de oliva** _3 cucharadas_
11. **Sal** _al gusto_
12. **Paprika** _1 cucharadita_

![pulled]({{ url.site }}/images/pulled-2.png "pulled")

## Para el pulled mushroom
~ 30 minutos

1. **Hongos** _250 gramos_ no botes los tallos.
2. **Sal** _al gusto_
3. **Hierbas aromáticas** _las que tengas: albahaca, tomillo, laurel. Al gusto_

## Para la ensalada
~ 10 minutos _o menos_

1. **Espinaca** _3 hojas_
2. **Zanahoria** _1/2 si quieren mucha o 1/3_

## Para armar

*Pan brioche o de hamburguesa o alguno redondido o el que tengas a la mano.* _2 unidades si es brioche o cuatro lonjitas si es de molde_

**Salsa BBQ**

* Lava los tomates y partelos en cuartos. Espolvorea sal, pimienta y aceite de oliva. Haz lo mismo con los dientes de ajo y la cebolla si decidiste incluirla.

* Rostiza los tomates y ajos en un horno —o en un sartén—, te tomará, a fuego alto, unos 20 minutos, revísalos cada tanto para que no se quemen.

* Cuando los tomates y los ajos estén listos licúalos con un chorrito de agua. No olvides la cebolla jugosa también.

* En un sartén hondo mezcla la salsa de soya, el vinagre de manzana, el vinagre blanco, el agua, el azúcar y la paprika y remueve vigorosamente. Incluye la salsa licuada a la olla.

* Cocina a fuego alto hasta que rompa hervor, luego baja el fuego y cocina por 15 minutos más. Si la quieres más espesa, cocina más tiempo.

* Retírala del fuego y deja enfriar a temperatura ambiente.

* Guárdala en un recipiente hermético en la nevera.


**Pulled mushroom**

* Limpia los hongos con una toallita de papel limpia. Separa los tallos del cuerpo, no los botes. Yo usé champiñones normales, puedes utilizar orellanas, shitakes, o un mix de los tres.

* Mezcla los hongos y sus tallos con las hierbas aromáticas de tu preferencia, sal y aceite de oliva.

* Ponlos en el horno durante 15 minutos

* Cuando estén todavía jugosos, sácalos y dejalos enfriar un tantito en alguna refractaria.

* Despedaza con las manos tanto los tallos como el cuerpo de los hongos. Y reserva.

* Vuelca la cantidad de salsa BBQ que prefieras sore los hongos y cocina durant 10 minutos.

**Ensalada**

* Limpia las hojas de las espinacas y la zanahoria

* Corta las hojas de las espinacas en tiritas y ralla la zanahoria.

* Mezcla con un poquito de aceite de oliva.


**Para juntarlo todo**

* Tuesta ambas caras del pan.

* Pon los hongos dentro del pan y luego la ensalada.

La receta clásica incluye ensalada de coleslaw, esto es un mix y seguro habrán más mixes. Todo funciona.

**Ñam.**
