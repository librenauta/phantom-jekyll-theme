---
layout: default
title:  "Salsa"
date:   2020-02-11 12:10:00 -0400
categories: jekyll update
image: /images/pic01.jpg
---
## Fideos con crema y hongos

**tiempo ~ 25min** | 2 Personas **by cyc**

_Receta **no** Vegana_

1. **Fideos**  _Mostachol o tirabuzon, 500gr_
2. **hongos de pino secos** _25 grs_
3. **cebolla** _1 grande_
4. **ajo** _1 ajo mediano_
5. **aceite** _2 chorritos (para saltear)_
6. **crema** _150ml_
7. **nuez moscada** _en polvo o para rallar, una pizca_
8. **sal** _una pizca_
9. **romero** _una pizca y media_
## :)
![hong]({{ url.site }}/images/hong-01.jpg "hong")

* Antes de comenzar, remojar los 25 gr de hongos secos de pino en agua tibia, por al menos 20 minutos, hasta poder cortarlos fácilmente.

* Comenzamos poniendo agua a hervir para cocinar ~300gr de fideos, podemos agregar 3 pizcas de sal al agua o a gusto.

* Luego en tabla de madera, cortar la cebolla y picar en cuadraditos y colocar en una sartén con 2 chorritos de aceite.

* Volcamos los fideos para cocinarlos mientras preparamos la salsa.

* Tomamos el ajo, lo pelamos y lo cortamos en 4, sacandole el eje interior (que es la parte que te deja el aliento feo, diría mi madre) y lo colocamos en la sartén junto a la cebolla.

* Luego de saltear por 5/7 minutos y que la cebolla esté transparente, cortamos y colocamos los hongos cortados en  cuadraditos y los 150ml de crema.

* Mezclamos un poco y agregamos nuez moscada, sal y romero según las cantidades indicadas. Esperamos unos 5 minutos y apagamos, así no se evapora la crema.


**Disfrutar, si es posible con pan casero <3  miam.**
