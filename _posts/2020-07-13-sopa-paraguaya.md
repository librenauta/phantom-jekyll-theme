---
layout: default
title:  "Sopa paraguaya"
date:   2020-07-13 11:50:00 -0400
categories: jekyll update
image: /images/sopa-p1.jpg
---

## Sopa  Paraguaya

**tiempo ~35 minutos** | 2 personas **by CyC**

_receta **no** vegana_

1. **Cebolla** _ 3 cebollas medianas_
2. **Queso Cuartirolo** _250gr_
3. **Leche** _250ml_
4. **Huevos** _3 huevos_
5. **Harina de maiz** (no es polenta, es más fina) _200gr_
6. **Manteca** _50gr_
7. **Polvo para hornear** _1 cucharada chiquita de té_
8. **Sal** _una pizca_

_esta cómida la probé por primera vez en una F.L.I.A, a un vendedor ambulante_

![sopa]({{ url.site }}/images/sopa-p1.jpg "sopa")

![sopa]({{ url.site }}/images/sopa-p2.jpg "sopa")

* Primero cortar las cebollas en juliana y por la mitad, dejando unas tiritas de ~ 2cm de largo, dorar a fuego lento y agregarle sal, dejar enfriar.

* En un recipiente rayar el queso y mezclarlo con los 3 huevos, cuando tenemos la cebolla fria, unimos estos 3 ingredientes.

* Calentamos la manteca hasta dejarla líquida y la dejamos que enfrie un poco, (este será el último ingrediente que agregaremos a nuestra mezcla)

* A la mezcla anterior sumamos los 200gr de harina de maiz junto con la cucharadita de polvo para hornear y comenzamos a aglutinar, para esto agregaremos la leche y si ya tenemos la manteca líquida pero tibia, agregamos al final.

* toda esta mezcla la volcamos en una en una asadera y horneamos.

![sopa]({{ url.site }}/images/sopa-p3.jpg "sopa")

*ÑIAM*
