---
layout: default
title:  "Pasta frola con Membrillo"
date:   2020-06-10 12:30:00 -0400
categories: jekyll update
image: /images/pasta-frola3.jpg
---
## Pasta frola con Membrillo <3

**tiempo ~ 60min** |  6 personas **by Cian**

_Receta **no** Vegana_

--sólido
1. **Harina 000**  _300 gr_
2. **azúcar** _100 gr_
3. **Maizena (fécula de maiz)** _100 gr_
4. **Bicarbonato** _una cucharada_
5. **Sal** _una pizca_
6. **Manteca** _150 gr_
7. **Membrillo** _500 gr_

--líquido
7. **Huevo** _2 cant_
8. **Leche** _1/2 de vaso_


![pasta]({{ url.site }}/images/pasta-frola3.jpg "pastafrola")

### Masa base

* Mezclar en un recipiente los ingredientes secos: azucar, harina, maizena, bicarbonato y sal.

* Luego agregar la manteca 150 gr e integrar con una espatula hasta formar un arenado (no amasar).

* a esta mezcla vamos a incorporar 1 huevo y 1 yema + 1/2 de vaso de leche.

* integrar todos los ingredientes sin amasar hasta tener una masa homogenea.

* dejar descansar en la heladera por 1/2 hora y después estirar con palo de amasar. cortar tiras para decorar arriba del dulce.

### Dulce

* poner dulce de membrillo en un tarrito a baño maria, hasta que se transforme en una pasta semiliquida (pero con consistencia para poder volcarla)

![pasta]({{ url.site }}/images/pasta-frola2.jpg "pastafrola")

![pasta]({{ url.site }}/images/pasta-frola1.jpg "pastafrola")

**~Miam, Disfrutar\<3**
