---
layout: default
title:  "Pastel de papas"
date:   2020-02-18 13:10:00 -0400
categories: jekyll update
image: /images/pastel-lentejas1.jpg
---
## Pastel de papa

**tiempo ~40min** | 2 personas **by cyc**

_Receta Vegana_

#### Relleno

1. **Lentejas** _1 lata o remojar y hervir previamente 1 taza_
2. **Zanahoria** _1 medianas_
3. **Cebolla** _2 medianas_
4. **Morrón (rojo)** _1 entero_
5. **Ajo** _1/2 diente_
6. **Peregíl** _un puñadito_
8. **Pimienta de cayena** _1 cucharadita_
9. **Pimentón** _1 cucharadita_
10. **Aceite** _para el salteado (chorrito)_
11. **sal** _1 cuacharadita_

#### Puré

1. **Papas** _4 papas medianas_
2. **Nuez moscada** _rallar un poquito_
:)

![imagen]({{ url.site }}/images/pastel-lentejas1.jpg)

![imagen]({{ url.site }}/images/pastel-lentejas2.jpg)

este es EL pastel de "papa" original de la plata vegano :P

* Remojar lentejas por medio día y cocinar hasta que estén blanditas (pinchar con tenedor).

* Pelar y cortar las papas para hervir. luego dejarlas en el agua caliente hasta que tengamos el relleno terminado y recién ahí armar el puré con nuez moscada, pizca de sal.

* Cortar en tiras cortas las verduras ( zanahoria, morrón, cebolla) y saltear junto con el ajo picado finito y el peregíl.

* Unir las lentejas con el salteado de verduras y poner en la base de una fuente, luego tapar con el puré.

![imagen]({{ url.site }}/images/pastel-lentejas3.jpg)

**~Disfrutar**
