---
layout: default
title:  "Falafel RMX"
date:   2020-02-15 11:10:00 -0400
categories: jekyll update
image: /images/fala-01.jpg
---
## Falafel remix

**tiempo ~ 40min** | 2 Personas **by cyc**

_Receta Vegana_ (con otro acompañamiento)

 1. **garbanzos**  _1 taza y media_
 2. **harina de garbanzo** _5 cucharadas grandes_
 3. **agua**_3 cucharadas grandes_
 4. **cebolla** _1/2 cebolla grande_
 5. **ajo** _1 ajo mediano_
 6. **cilantro** _1/2 taza_
 7. **nuez moscada** _en polvo o para rallar, una pizca_
 8. **sal** _2 pizcas_
 9. **aceite** _aceite como para freír las bolitas de falafel_

 ### Acompañamiento
 10. **pan árabe**_opcional para comer_
 11. **yogurt neutro / crema** _a gusto al comer_
 12. **repollo**_3 tiras finitas_
 13. **tomate cherry** _cortar varios por la mitad_

 ## :)
 ![falafel]({{ url.site }}/images/fala-01.jpg "fa")


 * Remojar los garbanzos por al menos 24hs en la heladera **importante**

 * Con los garbanzos ya hidratados y crudos procesarlos junto con: 1/2 cebolla, ajo, cilantro, sal y nuez moscada.

 * Luego volcar todo lo procesado en un recipiente y mezclar junto con la harina y el agua.

 * Poner a calentar el aceite para freír.

 * Con la pasta de garbanzos mezclada armar bolitas con la mano y apoyarlas en una espumadera para poder dejarlas en el aceite.

 * Freír hasta que estén doradas y colocar en una fuente con una servilleta de base.

 * Armar pan árabe con un corte en el extremo y abriendo el interior para poner: repollo, tomate , los falafel's y yogurt neutro o crema

**Disfrutar,  <3  miam.**
