---
layout: default
title:  "Pan"
date:   2020-01-09 12:48:00 -0400
categories: jekyll update
image: /images/pan3.jpg
---
## Pan Casero <3

 **tiempo ~ 60min** | **by cyc**

_receta vegana_ 

1. **harina 000**  _1 Kg_
2. **sal** _1 cucharada_
3. **aceite** _~150ml_
4. **levadura** _50gr (cubito)_
5. **agua tibia** _1-1/2 de agua tibia_
6. **salvado** _de acuerdo a pasos_
7. **molde de pan** _opcional_
8. **azucar** _de acuerdo a pasos_

## :)
![pancito]({{ url.site }}/images/pan0.jpg "pancito")

![pancito]({{ url.site }}/images/pan1.jpg "pancito")
fotos de pan de ~cian

![pancito]({{ url.site }}/images/pan2.jpg "pancito")

![pancito]({{ url.site }}/images/pan3.jpg "pancito")

_Pasos by librenauta:_
* Volcar la harina + el salvado en la mesada y crear un volcán único e irrepetible, como lava pondremos la levadura con un poco de agua tibia (hasta cubrirla) y dejaremos actuar por 5 minutos.

* Luego junto con el agua agregar un chorrito de aceite y en la base de nuestro volcán la sal. Evitar mezclar con la levadura de primera mano.

* Luego comenzamos a unir desde adentro y sumando cada vez más harina del volcán a la mezcla, a medida que combinamos esto, agregamos agua y aceite para lograr tener toda la masa homogénea.

* El punto justo de la masa está, cuando podemos ver todos los ingredientes combinados y sentimos la mezcla húmeda.

* Dejar en un contenedor tapado con una bolsa. reposar por 25 minutos.

* Preparar los bollitos para hornear. dejar en el horno hasta que se doren.

* Al sacarlo calentito, ponerle un poquito de agua y taparlo para que quede humedo.

**miam, Disfrutar con mermelada o mayonesa de zanahoria**
