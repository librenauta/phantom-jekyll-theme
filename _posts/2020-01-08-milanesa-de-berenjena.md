---
layout: default
title:  "Milanesas"
date:   2020-01-08 21:27:00 -0400
categories: jekyll update
image: /images/mila-beren1.jpg
---
## Milanesas de berenjena (milajenas)

**tiempo ~ 25min ** | 2 personas **by cyc**

_receta Vegana_

1. **berenjenas** _2_
2. **sal** _una pizca_
3. **ajo** _1 diente pequeño_
4. **perejil** _un puñadito_
5. **pimienta negra en polvo** _una pizca_
6. **aceite** _2 chorritos_
7. **harina de garvanzo** _2 cucharadas grandes_
8. **agua** _200ml_
9. **pan rallado**_250gr_

## :)

![mila]({{ url.site }}/images/mila-beren1.jpg "milanesas")

* Cortar 1 berenjena en 8/9 fetas a lo largo para luego taparlas con un poco con sal y dejarlas reposar por 10 minutos (con esto le sacamos el ácido) para luego escurrirlas en agua y rebosarlas.

* En una cubeta ponemos la harina de garbanzo junto con el agua y picamos medio puñadito de perejil + 1 diente de ajo pequeño (como el dedo indice)+ pizca de pimienta. batimos con tenedor hasta no tener grumos y tener una consistencia líquida pero con cuerpo.

* En un plato o cubeta tiramos pan rallado, para tapar vuelta y vuelta las berenjenas que pasan primero por la cubeta con el huevo.

* Aceitar una fuente para horno y poner las milanesas allí.

* Hornear hasta dorar y disfrutar.

![mila]({{ url.site }}/images/mila-beren2.jpg "milanesas")

![mila]({{ url.site }}/images/mila-beren3.jpg "milanesas")
