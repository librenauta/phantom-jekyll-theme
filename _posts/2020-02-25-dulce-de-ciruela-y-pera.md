---
layout: default
title:  "Dulce"
date:   2020-02-25 13:10:00 -0400
categories: jekyll update
image: /images/ciru-01.jpg
---
## Dulce de Ciruela y Pera

**tiempo ~ 1.5hs** | 3.5 frascos **by soycian**

_Receta Vegana_

1. **ciruelas**  _1.2kg sin caroso_
2. **azucar** _1 frasco_
3. **pera** _1 pera grande_
4. **limón** _dos gajos exprimidos _
5. **recipiente** _de plástico_
6. **olla de fondo ancho** _al estilo essen_
7. **frascos** _3 de vidrio_
8. **algodón** _un puñadito_
## ->
![ciruelas]({{ url.site }}/images/ciru-01.jpg "ciruelas 1")
![ciruelas]({{ url.site }}/images/ciru-02.jpg "ciruelas 2")

* Pelar la pera y cortarla en cubos de 1cm x 1cm.
* Lavar bien bien las ciruelas, y cortarlas sin pelarlas eliminando el carozo. exprimir 2 gajos de limón y luego agregar la taza de azúcar todo dentro del recipiente.
* Dejar reposar en la heladera por al menos 1hs, si se puede más mejor.
* Mientras esterilizar los 3 frascos con alcohol y un algodón, prestando especial atención a la boca.
* Vocar el contenido del recipiente en la olla y colocar a fuego mínimo con olla con un gajo destapada durante 45 minutos.
* Revolver un poco cada 20 minutos.
* Para verificar cuando está nuestro dulce, podemos tomar con una cuchara un poco de almíbar y depositarlo en un plato. luego pasar el dedo por el almíbar separándolo en 2 islas, si no se junta ya tenemos nuestro dulce cocinado <3
* Sacar de fuego y aplastar la fruta en caliente con un pisa papas.
* Envasar en caliente, limpiar el excedente en la boca de los frascos, para no dejar restos que puedan contaminar la conserva. Tapar y dar vuelta el frasco para que selle al vacío.
**Disfrutar,  <3  miam.**

![ciruelas]({{ url.site }}/images/ciru-03.jpg "ciruelas 3")
