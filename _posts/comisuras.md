
##COMISURAS

## Falafel
>tiempo ~ 60min
>personas 2

1. grabanzos | 1/4kg
2. 1 diente de ajo
3. cilantro
4. perejil
5. aceite
6. sal
7. yougurt
8. repollo
9. procesadora (o minipimer con picador)

-- Remojar el día anterior los garbanzos para poder procesarlos mejor
-- Generar una mezclolansa con cilantro picado + ajo picadito
--


## Empanadas 
# masa de tapa de empanada (nivel goku)
1. harina
2. levadura
3. sal



# de Capresse
>tiempo ~ 25min
>personas 2

1. tomate (2)
2. albahaca (1 ramo)
3. queso cremoso o muzarella (350gr)
4. tapas de empanadas (12)
5. pocillo con agua.
-- Cortar en cubitos el tomate y limpiar en agua bien las hojas de albahaca
-- Luego armar las empanadas con un cubo de queso o muzarella y el tomate y albahaca
-- Usar el pocillo con agua para con el dedo indice mojar todo el borde de la tapa de empanada (esto permite que se cierre mejor)
-- en una fuente para horno poner pan rallado creando un piso donde veamos la fuente y estrellas de pan rallado, luego poner las empanadas allí y disfrutar.
(el pan rallado genera una película que permite que no se quemen ni se peguen las empanadas)


# de soja texturizada (textuempanadas)
1. soja texturizada 
2. cebolla
3.


## Milanesas de berenjena
>tiempo ~ 25min
>personas 2

1. berenjenas
2. sal
3. ajo
4. perejil
5. pimienta
6. aceite
7. 1 huevo
8. pan rallado

-- Cortar 1 berenjena en 8/9 fetas a lo largo para luego taparlas un poco con sal y dejarlas reposar por 10 minutos (con esto le sacamos el ácido)
-- En una cubeta ponemos 1 huevo y picamos medio pñadito de perejil + 1 diente de ajo pequeño (como el dedo indice)+ pizca de pimienta. batimos con tenedor por 2 minutos.
-- En un plato o cubeta tiramos pan rallado, para tapar vuelta y vuelta las berenjenas que pasan primero por la cubeta con el huevo.
-- Aceitar una fuente para horno y poner las milanesas allí. 
--hornear hasta dorar y disfrutar.
