---
layout: default
title:  "Torta invertida"
date:   2020-01-09 12:50:00 -0400
categories: jekyll update
image: /images/pic01.jpg
---
## Torta de Manzana

**tiempo ~ 40min**   | 4 Personas **by cian**

_Receta **no** Vegana_

1. **huevos**  _3_
2. **azúcar** _una taza_
3. **escencia de vainilla** _una cucharadita_
4. **aceite** _1/3 de taza_
5. **harina** _1-1/2  taza_
6. **polvo de hornear** _una cucharada_
7. **canela en polvo** _una cucharadita_
8. **fuente con isla en el centro** _como para flan_

## :)
![manz]({{ url.site }}/images/manz-01.jpg "manz")

* (prontito descripción )


**Disfrutar, miam.**
