---
layout: default
title:  "Quesito de papa"
date:   2020-07-20 11:50:00 -0400
categories: jekyll update
image: /images/queso-1.jpg
---

## Quesito de PAPA

~30 minutos  | 2 personas | **by @sofitimartinez**

_Receta Vegan_

## Para el quesito:

1. **Papas** _2 o 3  medianas_
2. **Aceite de Oliva** _Un chorrito_
3. **Levadura nutricional** _en copos o sabor queso (no es necesario que sea la saborizada)_
4. **Sal** _(opcional)_
5. **pimienta** _(opcional)_
6. **Cúrcuma** _(opcional)_
7. **Ajo** _1 diente_
8. **Minipimer**

![queso]({{ url.site }}/images/queso-1.jpg "pizza")

**Procedimiento!**

* Pelar y cortar las papas en cuadraditos medios peques

* Ponerlas en una ollita con agua que apenas las tape, la idea es después no escurrir el agua y que las papas se vayan ablandando en esa misma agua

* Poner las papas en el fuego con un diente de ajo crudo (sacandole el cabito del medio) hasta que hierva e ir chequeando que se pongan blanditas

* Una vez que están a punto como para hacer un puré, apagar el fuego y si es necesario sacarles agua, no tienen que estar escurridas del todo pero tampoco tiene que verse agua sin papa capice?

* Ahora como si hiceras un puré de papas, aplastalas con un tenedor, agregá sal, pimienta, aceite de oliva o girasol un buen chorro, yo le pongo un poco de cúrcuma tambien para que tenga mas colorcito y levante el sabor. Tambien agregar dos cucharadas soperas de levadura nutricional

* Mezclar todo mientras seguis aplastando, y ahora si, usar una minipimmer en este momento es casi muy importante, mixeas todo eso, y va a empezar a ponerse cremoso y medio pegajoso

* Listo! deja enfriar un poquito o no y ya podés ponerselo a una pizza, empanadas, u lo que quieras!

![queso]({{ url.site }}/images/queso-2.jpg "pizza")

**~miam**
