---
layout: default
title:  "Milanesas"
date:   2020-01-09 12:50:00 -0400
categories: jekyll update
image: /images/zucc-03.jpg
---
## Milanesas de Zucchini (milascchinis) <3

**tiempo ~ 40min** | 3 Personas **by cyc**

_Receta **no** Vegana_

1. **zucchini**  _3 medianos_
2. **sal** _una pizca_
3. **huevo** _2_
4. **perejil** _un puñadito_
5. **pimienta blanca en polvo** _una pizca_
6. **pimienta de cayena** _dos pizcas_
7. **pan rallado** _250gr_
8. **recipiente hondo** _~20cm de diámetro _

## :)
![zucc]({{ url.site }}/images/zucc-03.jpg "zucc")

* Comenzar por los zucchinis, despuntar y luego cortarlos en fetas a lo largo sobre una tabla de madera, con un espesor entre 5 y 10 mm (si son muy finitas se queman, si son muy gruesas cuesta que se cocinen)

* Luego en el recipiente, volcar 2 huevos enteros, picar ajo y perejil bastante chiquitito, agregar ambas pimientas y mezclar con un tenedor.

* Por último rebozar los zucchinis en la mezcla del recipiente y preparar un plato hondo con pan rallado.
posar las milanesas en una fuente de horno con aceite (ponerle un chorrito de aceite en la parte superior de cada milanesa). dar 30 min de horno, controlarlas y  darlas vuelta cuanto se doren de un lado.

**Disfrutar, miam.**
