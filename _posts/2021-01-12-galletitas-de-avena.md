---
layout: default
title:  "Galletitas de Avena"
date:   2021-01-12 14:30:00 -0400
categories: jekyll update
image: /images/g-avena-2.jpg
---
## Galletitas de avena nevadas con azúcar

**tiempo ~ 2hs ** | 6 personas **by cian**

(varias tandas de horneado) _ideal para cumpleaños_

_Receta Vegana_ <3

> _elegí una taza en tu casa y medí todos los ingredientes con la misma_

### Ingredientes secos
1. **avena instantánea** _2 tazas_
2. **azúcar común** _1 taza_
3. **azúcar integral (rubia o mascabo)** _1/2 taza_
4. **harina 000** _1 taza_
5. **harina integral** _1 taza_
6. **pasas de uva** _1/2 taza_
7. **coco rallado** _1/2 taza_
8. **canela** _1 cucharada grande (colmada)_
9. **polvo leudante** _1 cucharadita (colmada)_
10. **azúcar impalpable** _cantidad necesaria para espolvorear_
11. **sal** _1 cucharadita (colmada)_

### Ingredientes húmedos
1. **leche vegetal (común para no vegane)** _3 tazas_
2. **banana** _1 grande o 2 pequeñas_
3. **aceite** _1/2 taza_


## :O

![galletitas-avena]({{ url.site }}/images/g-avena-1.jpg "galletitas")

## bueno esto es delicioso x 2.

* Mezclar todos los ingredientes secos en un recipiente grande

* Aparte pisar la banana y agregar aceite para unir y poner en el centro de la preparación anterior

* Entibiar la leche e incorporar mezclando con un tenedor para que no queden grumos hasta formar una pasta

* Dejar reposar  20 minutos para que la avena absorba bien el líquido.

* Volver a agregar leche en caso de haberse secado, tiene que quedar una consistencia viscosa pero
sin rastros de líquido.

* Armar las galletas con una cuchara directamente sobre la superficie a hornear ,previamente enharinada la base.

* Hornear hasta dorar
