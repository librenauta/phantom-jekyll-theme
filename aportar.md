---
layout: page
title: Aportar recetitas
---
## Suma una receta <3
podés sumar recetas en el siguiente [**link**](https://pad.riseup.net/p/comisuras-keep), un pad (texto colaborativo) o también si tenés cuenta en gitlab y querés aportar post directamente podés abrir una request [**Aquí**](https://gitlab.com/librenauta/phantom-jekyll-theme).
Para enviarme imágenes podés enviarmelas vía [**telegram**](https://t.me/librenauta).
