---
layout: page
title: qué es esto?
featured-image: images/pasta-frola3.jpg
---

Comisuras fue nombrado por un **autocorrect**, el día que quise escribir "comiditas" en un mensaje de txt, y se trata de una base de recetas que queremos _compartir_ (cyc) , porque creemos que las recetas tienen una linea muy ligada a la cultura libre, siempre fueron compartidas, modificadas  y  reapropiadas.
